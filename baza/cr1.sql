-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 04 Lut 2022, 22:16
-- Wersja serwera: 10.4.21-MariaDB
-- Wersja PHP: 8.0.11


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `shop`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pytania`
--
-- Zrzut danych tabeli `pytania`
--

INSERT INTO `odpowiedzi` (`ID`, `OdpA`, `OdpB`, `OdpC`, `OdpD`) VALUES
(1, '$ocena > 2 and $ocena < 6', '$ocena > 2 or $ocena < 6', '$ocena >= 2 or $ocena <= 6', '$ocena >= 2 and $ocena <= 6'),
(2, 'setcookie(\"dane\", $dane, 0);', ' setcookie(\"dane\", \"dane\", 0);', ' setcookie(\"dane\", $dane, time());\r\n', ' setcookie(\"dane\", $dane, time() + (3600*24)); '),
(3, 'sesji.', 'ciasteczek.', 'formularza.', 'bazy danych.'),
(4, 'rok.\r\n', 'dzień.\r\n', 'miesiąc.', 'Żadna ze wskazanych.'),
(5, '#', '//', '/* */', '<!-- -->'),
(6, 'klucz główny będzie zawsze liczbą całkowitą.\r\n', ' każdej kolumnie zostanie przypisany typ danych.', 'dla każdej tabeli zostanie utworzony klucz główny', 'każdy klucz główny będzie miał odpowiadający mu klucz obcy w innej tabeli.'),
(7, 'UPDATE', 'INSERT INTO\r\n', 'ALTER TABLE\r\n', 'CREATE TABLE\r\n'),
(8, 'zsumować koszt wszystkich usług.', 'wskazać najwyższą cenę za usługi.', 'policzyć ile jest usług dostępnych w tabeli.', 'obliczyć średnią arytmetyczną cen wszystkich usług.'),
(9, 'Krzysztof, Krystyna, Romuald', 'Rafał, Rebeka, Renata, Roksana.', 'Gerald, Jarosław, Marek, Tamara.', 'Arleta, Krzysztof, Krystyna, Tristan.'),
(10, ' pogrupowane. ', 'występujące w bazie tylko raz.\r\n', 'posortowane malejąco lub rosnąco.', 'tak, aby w podanej kolumnie nie powtarzały się wartości.'),
(11, 'DATE', 'YEAR', 'BOOLEAN\r\n', 'TIMESTAMP'),
(12, 'raportem.', ' formularzem.\r\n', 'filtrowaniem.\r\n', 'kwerendą SELECT.'),
(13, 'INSERT INTO\r\n', 'ALTER COLUMN', 'CREATE COLUMN', 'TRUNCATE TABLE'),
(14,  'usunąć kolumnę cena typu float.\r\n', 'zmienić typ na float dla kolumny cena.', 'zmienić nazwę kolumny z cena na float.', 'dodać kolumnę cena o typie float, jeśli nie istnieje.'),
(15, 'CREATE USER \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';', 'CREATE USER OR DROP \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';\r\n', 'CREATE OR REPLACE USER \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';', 'CREATE USER IF NOT EXISTS \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';\r\n'),
(16, 'wybierania, wstawiania oraz aktualizacji danych tabeli o nazwie klienci.', 'wybierania, dodawania pól oraz zmiany struktury tabeli o nazwie klienci.', 'wybierania, wstawiania oraz aktualizacji danych wszystkich tabel w bazie o nazwie klienci.', 'wybierania, dodawania pól oraz zmiany struktury wszystkich tabel w bazie o nazwie klienci.'),
(17, 'CREATE USER \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';', 'CREATE USER OR DROP \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';\r\n', 'CREATE OR REPLACE USER \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';', 'CREATE USER IF NOT EXISTS \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';\r\n'),
(18, 'wybierania, wstawiania oraz aktualizacji danych tabeli o nazwie klienci.', 'wybierania, dodawania pól oraz zmiany struktury tabeli o nazwie klienci.', 'wybierania, wstawiania oraz aktualizacji danych wszystkich tabel w bazie o nazwie klienci.', 'wybierania, dodawania pól oraz zmiany struktury wszystkich tabel w bazie o nazwie klienci.'),
(19, 'CREATE USER \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';', 'CREATE USER OR DROP \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';\r\n', 'CREATE OR REPLACE USER \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';', 'CREATE USER IF NOT EXISTS \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';\r\n'),
(20, 'wybierania, wstawiania oraz aktualizacji danych tabeli o nazwie klienci.', 'wybierania, dodawania pól oraz zmiany struktury tabeli o nazwie klienci.', 'wybierania, wstawiania oraz aktualizacji danych wszystkich tabel w bazie o nazwie klienci.', 'wybierania, dodawania pól oraz zmiany struktury wszystkich tabel w bazie o nazwie klienci.'),
(21, 'otrzymania id ostatnio wstawionego wiersza.', 'otrzymania kodu błędu, gdy proces wstawiania wiersza się nie powiódł.', 'pobrania najwyższego indeksu bazy, aby po jego inkrementacji wstawić pod niego dane.\r\n', 'pobrania pierwszego wolnego indeksu bazy, tak, aby można było pod nim wstawić nowe dane.'),
(22, ' <i> oraz <mark>', ' <u> oraz <sup>', ' <b> oraz <i>', '<b> oraz <u>'),
(23, '{background-image: url(\"rys.png\"); background-repeat: round;}\r\n', '{background-image: url(\"rys.png\"); background-repeat: repeat;}\r\n', '{background-image: url(\"rys.png\"); background-repeat: repeat-x;}\r\n', '{background-image: url(\"rys.png\"); background-repeat: repeat-y;}\r\n'),
(24,  'każdy tekst w znaczniku <p> lub każdy tekst w znaczniku <i>', 'każdy tekst w znaczniku <p> za wyjątkiem tych w znaczniku <i>\r\n', 'jedynie ten tekst znacznika <p>, do którego jest przypisana klasa o nazwie i\r\n', 'jedynie ten tekst w znaczniku <i>, który jest umieszczony bezpośrednio wewnątrz znacznika <p>'),
(25, 'jeśli jest to pierwsze wystąpienie tego znacznika w dokumencie.\r\n', 'gdy zostanie wskazane kursorem myszy bez kliknięcia.', 'po kliknięciu myszą w celu zapisania w nim tekstu.', 'w każdym przypadku.'),
(26, '#0000EE', '#EE0000', '#00EE00', '#EE00EE\r\n'),
(27, 'echo(ini_get_all());', 'echo phpversion();\r\n', ' phpcredits();', ' phpinfo();\r\n'),
(28, 'let', 'new', '$', 'begin'),
(29, 'inkrementacyjny.\r\n', 'podstawowy.', ' przestawny.', 'obcy.\r\n'),
(30, 'UPDATE', 'TRUNCATE\r\n', 'DROP TABLE', 'ALTER TABLE'),
(31, 'klucza podstawowego.', 'użycia atrybutu DEFAULT.\r\n', 'definicji wszystkich pól tabeli.', 'definicji wszystkich pól typu numerycznego.'),
(32, 'raport.', 'relację.', 'formularz.', 'makropolecenie.'),
(33, 'DROP TABLE usuwa tabelę, a TRUNCATE TABLE modyfikuje w niej dane spełniające \r\nwarunek.\r\n', ' DROP TABLE usuwa tabelę, a TRUNCATE TABLE usuwa wszystkie dane, pozostawiając \r\npustą tabelę.\r\n', 'Obydwa polecenia usuwają jedynie zawartość tabeli, ale tylko polecenie DROP TABLE może \r\nbyć cofnięte.\r\n', 'Obydwa polecenia usuwają tabelę wraz zawartością, ale tylko polecenie TRUNCATE TABLE \r\nmoże być cofnięte'),
(34, 'GRANT', 'SELECT ', 'CREATE', 'ALTER'),
(35, 'INSERT INTO', 'UPDATE\r\n', 'SELECT', 'ALTER'),
(36, '<tr>', '<ul>', '<td>', '<br>'),
(37, 'hight ', 'margin', 'padding', 'width'),
(38, 'C#\r\n', 'Perl', 'PHP\r\n', 'JavaScript');

----------------------------------------------------------