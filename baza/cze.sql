-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 04 Lut 2022, 22:16
-- Wersja serwera: 10.4.21-MariaDB
-- Wersja PHP: 8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `shop`
--

-- --------------------------------------------------------
--
-- Zrzut danych tabeli `pytania`
--

INSERT INTO `pytania_2`(`ID`, `Pytanie`) VALUES
(1, 'Program zapisany w języku PHP ma za zadanie obliczyć średnią pozytywnych ocen ucznia od 2 do 6. \nWarunek wybierania ocen w pętli liczącej średnią powinien zawierać wyrażenie logiczne\n'),
(2, 'W jaki sposób, stosując język PHP, zapisać w ciasteczku napis znajdujący się w zmiennej dane na czas \r\njednego dnia?'),
(3, 'if (empty($_POST[\"name\"])) {\r\n$nameErr = \"Name is required\";\r\n} \r\nPrzedstawiony fragment kodu PHP służy do obsługi\r\n'),
(4, 'echo date(\"Y\");\r\nPo wykonaniu kodu PHP zostanie wyświetlona aktualna data zawierająca jedynie'),
(5, 'Który zapis definiuje w języku PHP komentarz wieloliniowy?\n'),
(6, 'Integralność encji w bazie danych zostanie zachowana, jeżeli między innymi\n'),
(7, 'Aby przy pomocy zapytania SQL zmodyfikować strukturę istniejącej tabeli, należy zastosować kwerendę\r\n'),
(8, 'SELECT AVG(cena) FROM uslugi;\r\nFunkcja agregująca AVG użyta w zapytaniu ma za zadanie\r\n'),
(9, 'SELECT imie FROM mieszkancy WHERE imie LIKE \'_r%\';\nKtóre imiona zastosowaną wybrane w wyniku tego zapytania?\n'),
(10, 'Kwerendę SELECT DISTINCT należy zastosować w przypadku, gdy potrzeba wybrać rekordy'),
(11, 'Którego typu danych w bazie MySQL należy użyć, aby przechować w jednym polu datę i czas?'),
(12, 'Aby edytować dane w bazie danych można posłużyć się'),
(13, 'Aby usunąć wszystkie rekordy z tabeli należy zastosować kwerendę'),
(14, 'ALTER TABLE artykuly MODIFY cena float;\r\nKwerenda ma za zadanie w tabeli artykuly'),
(15, 'W języku SQL, aby zabezpieczyć kwerendę CREATE USER tak, aby nie zostało utworzone konto \r\nw przypadku, gdy już istnieje, można posłużyć się składnią\r\n'),
(16, 'GRANT SELECT, INSERT, UPDATE ON klienci TO anna;\r\nZakładając, że użytkownik wcześniej nie miał żadnych praw, polecenie SQL nada użytkownikowi anna prawa \r\njedynie do'),
(17, 'W języku SQL, aby zabezpieczyć kwerendę CREATE USER tak, aby nie zostało utworzone konto \r\nw przypadku, gdy już istnieje, można posłużyć się składnią\r\n'),
(18, 'GRANT SELECT, INSERT, UPDATE ON klienci TO anna;\r\nZakładając, że użytkownik wcześniej nie miał żadnych praw, polecenie SQL nada użytkownikowi anna prawa \r\njedynie do'),
(19, 'W języku SQL, aby zabezpieczyć kwerendę CREATE USER tak, aby nie zostało utworzone konto \r\nw przypadku, gdy już istnieje, można posłużyć się składnią\r\n'),
(20, 'GRANT SELECT, INSERT, UPDATE ON klienci TO anna;\r\nZakładając, że użytkownik wcześniej nie miał żadnych praw, polecenie SQL nada użytkownikowi anna prawa \r\njedynie do'),
(21, 'Pole insert_id zdefiniowane w bibliotece MySQLi języka PHP może być wykorzystane do '),
(22, 'Znaczniki HTML <strong> oraz <em> służące do podkreślenia ważności tekstu, pod względem formatowania \r\nsą odpowiednikami znaczników'),
(23, 'W języku CSS, należy zdefiniować tło dokumentu jako obraz rys.png. Obraz ma powtarzać się jedynie \r\nw poziomie. Którą definicję należy przypisać selektorowi body?'),
(24, 'W języku CSS zapis selektora p > i { color: red;} oznacza, że kolorem czerwonym zostanie \r\nsformatowany'),
(25, 'input:focus { background-color: LightGreen; }\r\nW języku CSS zdefiniowano formatowanie dla pola edycyjnego. Tak formatowane pole edycyjne będzie miało \r\njasnozielone tło '),
(26, 'Kolorem o barwie niebieskiej jest kolor'),
(27, 'Którym poleceniem można wyświetlić konfigurację serwera PHP, w tym informację m. in. o: wersji PHP, \r\nsystemie operacyjnym serwera, wartości przedefiniowanych zmiennych?'),
(28, 'Za pomocą którego słowa kluczowego deklaruje się zmienną w języku JavaScript?\r\n'),
(29, 'Pole lub zbiór pól jednoznacznie identyfikujący każdy pojedynczy wiersz w tabeli w bazie danych to klucz'),
(30, 'W języku SQL, aby zmienić strukturę tabeli, np. poprzez dodanie lub usunięcie kolumny, należy zastosować \r\npolecenie'),
(31, 'Atrybut kolumny NOT NULL jest wymagany w przypadku'),
(32, 'W bazach danych do prezentacji danych spełniających określone warunki nalezy utworzyć'),
(33, 'Wskaż różnicę pomiędzy poleceniami DROP TABLE i TRUNCATE TABLE.\r\n'),
(34, 'Aby nadać użytkownikowi uprawnienia do tabel w bazie danych, należy zastosować polecenie '),
(35, 'Aby przesłać dane za pomocą funkcji mysqli_query() w skrypcie PHP, który wstawia do bazy danych dane \r\npobrane z formularza ze strony internetowej, jako jednego z parametrów należy użyć kwerendy\r\n'),
(36, 'Który znacznik należy do znaczników definiujących listy w języku HTML?'),
(37, 'Której właściwości CSS należy użyć, aby zdefiniować marginesy wewnętrzne dla elementu?'),
(38, 'Globalne tablice do przechowywania danych o ciastkach i sesjach: $_COOKIE oraz $_SESSION są częścią \r\njęzyka');

--
-- Struktura tabeli dla tabeli `users`