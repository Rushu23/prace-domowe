<?php
    //Sprawdzenie czy użytkownik jest zalogowany oraz czy rozpoczął quiz
if(!isset($_SESSION)) 
    { 
        session_start();
    }
if((!isset($_SESSION['zalogowany'])) && ($_SESSION['zalogowany']!=true))
    {
        header('Location: login.php');
        exit();
    }
if(!isset($_GET['start']) | (!isset($_GET['ile'])))
    {
        header('Location: ile.php');
        exit();
    }
try
{
    //Połączenie z bazą i wyrzucenie ewntualnego błędu
    $connect = new mysqli($_SESSION['host'], $_SESSION['db_user'], $_SESSION['db_password'], $_SESSION['db_name']);

    if($connect->connect_errno!=0)throw new Exception(mysqli_connect_errno());?>
    <html>
    <head>
    <html lang="pl">
    <title>Mój quiz</title>
    <link href="style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>

        <div class="title">Quiz</div>
        <?php
        if(isset($_POST['clicks']) && isset($_GET['start']))
            {
                //Sprawdzenie czy użytkownik rozpoczął quiz oraz które ma pytanie
                @$_SESSION['clicks'] += 1;
                $c = $_SESSION['clicks'];
        
            if(isset($_POST['odp']))
                {
                //Zapisywanie odpowiedzi użytkownika w bazie danych
                $userselected = $_POST['odp'];
                $qry2 = $connect->query(sprintf("UPDATE `pytania` SET`odp`='%s' WHERE `id`='%s';",
                mysqli_real_escape_string($connect,$userselected),
                mysqli_real_escape_string($connect,$c-1)));
                if(!$qry2) throw new Exception($connect->error);
                }
            }
            else
            {
                $_SESSION['clicks'] = 1;
            }

        //echo($_SESSION['clicks']); ilość wykonanych pytań
        ?>
    <form action="" method="post">
    
    <?php
    $ilosc_pytan = $_GET['ile'];
    $c = $_SESSION['clicks'];
    if((isset($c)) && ($c<= $ilosc_pytan))//Sprawdzenie wybraniej ilości pytań
        {
            $qry = $connect->query(sprintf("SELECT * FROM `pytania` where id='%s';",mysqli_real_escape_string($connect,$c)));
            $num = mysqli_num_rows($qry);
            $row = mysqli_fetch_array($qry,MYSQLI_ASSOC);
            if(!$qry) throw new Exception($connect->error);?>
            <table class="odpy1">
            <tr><td><h2>Pytanie Nr.&nbsp;<?php echo htmlspecialchars($row['id']);?></h2></td></tr>
            <tr><td><h3><?php echo htmlspecialchars($row['pytanie']);?></h3></td></tr>
            <?php 
            if($c >= 0 && $c <= $ilosc_pytan)
                { ?>
                <tr><td class="radio-quiz"><input id="pyt1" required type="radio" name="odp" value="a">
                <label for="pyt1"><?php echo htmlspecialchars($row['odpA']); ?></label></td></tr>
                <tr><td class="radio-quiz"><input id="pyt2" required type="radio" name="odp" value="b">
                <label for="pyt2"><?php echo htmlspecialchars($row['odpB']); ?></label></td></tr>
                <tr><td class="radio-quiz"><input id="pyt3" required type="radio" name="odp" value="c">
                <label for="pyt3"><?php echo htmlspecialchars($row['odpC']); ?></label></td></tr>
                <tr><td class="radio-quiz"><input id="pyt4" required type="radio" name="odp" value="d">
                <label for="pyt4"><?php echo htmlspecialchars($row['odpD']); ?></label></td></table>
                <button class="button3" name="clicks">Następne pytanie</button></tr><?php
                }
        }
    ?>
    </form>
    <?php
        if($c>$ilosc_pytan)
            {
                $limitpytan = intval($ilosc_pytan);
                $qry3 = $connect->query(sprintf("SELECT `id`, `prawidlowa_odp`, `odp` FROM `pytania` LIMIT %s ;",mysqli_real_escape_string($connect,$limitpytan)));
                @$_SESSION['score'] = 0;
                if(!$qry3) throw new Exception($connect->error);
                while($row3 = mysqli_fetch_array($qry3, MYSQLI_ASSOC))
                {
                    if($row3['prawidlowa_odp']==$row3['odp'])
                        {
                            @$_SESSION['score'] += 1 ;
                        }
                }

    ?>
    <div class="napis_end">Koniec</div>
    <form action="wynik.php" method="post">
    <Button class="button_end" name="koniec" value="">Zakończ Quiz i poznaj wynik!</Button></form>
    <?php
            }

    $connect->close();
}
catch(Exception $er)
{
    echo'<span style="color:red;">Błąd serwera!
    Przepraszamy za niedogodności (naprawa soon!) albo zalogowałeś się na konto w bazie danych które ma niewystarczające uprawnienia do używania quizu! </span>';
    //echo '<br/>Informacja developerska: '.$er;
    session_unset();
}?>
</body>
</html>
