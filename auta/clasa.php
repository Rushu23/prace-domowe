<?php
class A{
protected static function method(){
	echo __CLASS__;
}
public static function staticTest(){
	static::method();
}

public static function selfTest(){
	self::method();
}
}
class B extends A{
	protected static function method(){
echo __CLASS__;
}
}
B::staticTest();
echo '<br>';
B::selfTest();
echo '<br>';
